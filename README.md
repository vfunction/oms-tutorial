# OMS Tutorial - source files

This respository is used by the [vFunction](http://vfunction.com) Order Management System (OMS) tutorial, as an example of how to modernize a Spring Web MVC application into a set of Spring Boot services using the vFunction platform.

There are two folders:

1. [oms-webmvc](./oms-webmvc) - the monolith Spring Web MVC application of an Order Management System, to be analyzed and decomposed to services. For more details see its [README](./oms-webmvc/README.md) file
2. [oms-services](./oms-services) - contains the resulting extraxcted services and common library, which are the end result of the tutorial. See its [REAMDE](./oms-services/README.md) for more details.